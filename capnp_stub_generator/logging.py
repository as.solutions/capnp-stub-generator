import functools
import logging
from typing import Any
from rich.logging import RichHandler
from rich.pretty import pretty_repr

import rich_click as click

LOG_LEVEL_TRACE = 5

logging.addLevelName(level=LOG_LEVEL_TRACE, levelName="TRACE")
logging.basicConfig(
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, tracebacks_suppress=[click])],
)


class Logger(logging.LoggerAdapter):
    _logger: logging.Logger = logging.getLogger("rich")

    def __init__(self) -> None:
        super().__init__(Logger._logger)

    def set_verbosity(self, verbose: int) -> None:
        match verbose:
            case 0:
                log_level = logging.INFO
            case 1:
                log_level = logging.DEBUG
            case _:
                log_level = LOG_LEVEL_TRACE

        self.setLevel(log_level)
        self.debug(f"set verbosity to {logging.getLevelName(log_level)}")

    def trace(self, msg, *args, **kwargs):
        """
        Delegate a trace call to the underlying logger.
        """
        self.log(LOG_LEVEL_TRACE, msg, *args, **kwargs)

    def add_trace(self, _func=None):
        def add_trace_info(func):
            @functools.wraps(func)
            def wrapper(other_self, *args, **kwargs) -> Any:
                self.trace(
                    f"enter {func.__name__} on: {other_self}\n"
                    f"\targs: {pretty_repr(args)}\n"
                    f"\tkwargs: {pretty_repr(kwargs)}"
                )
                result = func(other_self, *args, **kwargs)
                self.trace(
                    f"exit {func.__name__} {other_self}\n"
                    f"\tresult: {pretty_repr(result)}"
                )
                return result

            return wrapper

        if _func is None:
            return add_trace_info
        return add_trace_info(_func)


log = Logger()
