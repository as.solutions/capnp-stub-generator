"""Top-level module for stub generation."""

from __future__ import annotations

import glob
from rich.pretty import pretty_repr
import os.path
from types import ModuleType

import black
import capnp  # type: ignore
import isort

from capnp_stub_generator.capnp_types import ModuleRegistryType
from capnp_stub_generator.helper import replace_capnp_suffix
from capnp_stub_generator.logging import Logger
from capnp_stub_generator.writer import Writer

capnp.remove_import_hook()


log = Logger()

PYI_SUFFIX = ".pyi"
PY_SUFFIX = ".py"
LINE_LENGTH = 120


def _format_outputs(
    raw_input: str, is_pyi: bool, line_length: int = LINE_LENGTH
) -> str:
    """Formats raw input by means of `black` and `isort`.

    Args:
        raw_input (str): The unformatted input.
        is_pyi (bool): Whether or not the output is a `pyi` file.

    Returns:
        str: The formatted outputs.
    """
    # FIXME: Extract config from dev_policies
    sorted_imports = isort.code(
        raw_input, config=isort.Config(profile="black", line_length=line_length)
    )
    return black.format_str(
        sorted_imports, mode=black.Mode(is_pyi=is_pyi, line_length=line_length)
    )


def _generate_stubs(
    module: ModuleType, module_registry: ModuleRegistryType, output_file_path: str
):
    """Entry-point for generating *.pyi stubs from a module definition.

    Args:
        module (ModuleType): The module to generate stubs for.
        module_registry (ModuleRegistryType): A registry of all detected modules.
        output_file_path (str): The name of the output stub files, without file extension.
    """
    log.trace(f"generate_stubs \n{module}\n\t-> basepath: {output_file_path}")
    writer = Writer(module, module_registry)
    writer.generate_all_nested()

    for outputs, suffix, is_pyi in zip(
        (writer.dumps_pyi(), writer.dumps_py()), (PYI_SUFFIX, PY_SUFFIX), (True, False)
    ):
        formatted_output = _format_outputs(outputs, is_pyi)

        with open(output_file_path + suffix, "w", encoding="utf8") as output_file:
            output_file.write(formatted_output)

    log.info("Wrote stubs to '%s(%s/%s)'.", output_file_path, PYI_SUFFIX, PY_SUFFIX)


def run(
    clean: list[str],
    paths: list[str],
    excludes: list[str],
    recursive: bool,
    root_directory: str,
):
    """Run the stub generator on a set of paths that point to *.capnp schemas.

    Uses `generate_stubs` on each input file.

    Args:
        args (argparse.Namespace): The arguments that were passed when calling the stub generator.
        root_directory (str): The directory, from which the generator is executed.
    """
    cleanup_paths: set[str] = set()
    for c in clean:
        cleanup_directory = os.path.join(root_directory, c)
        cleanup_paths = cleanup_paths.union(
            glob.glob(cleanup_directory, recursive=recursive)
        )

    for cleanup_path in cleanup_paths:
        log.trace(f"Cleanup: removing {cleanup_path}")
        os.remove(cleanup_path)

    excluded_paths: set[str] = set()
    for exclude in excludes:
        exclude_directory = os.path.join(root_directory, exclude)
        excluded_paths = excluded_paths.union(
            glob.glob(exclude_directory, recursive=recursive)
        )

    search_paths: set[str] = set()
    for path in paths:
        search_directory = os.path.join(root_directory, path)
        search_paths = search_paths.union(
            glob.glob(search_directory, recursive=recursive)
        )

    # The `valid_paths` contain the automatically detected search paths, except for specifically excluded paths.
    valid_paths = search_paths - excluded_paths
    log.trace(f"Search paths: {pretty_repr(valid_paths)}")

    parser = capnp.SchemaParser()
    module_registry: ModuleRegistryType = {}

    for path in valid_paths:
        module = parser.load(path)
        module_registry[module.schema.node.id] = (path, module)

    log.trace(f"module_registry: {pretty_repr(module_registry)}")
    for path, module in module_registry.values():
        output_directory = os.path.dirname(path)
        output_file_name = replace_capnp_suffix(os.path.basename(path))

        _generate_stubs(
            module, module_registry, os.path.join(output_directory, output_file_name)
        )
