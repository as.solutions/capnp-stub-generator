"""Command-line interface for generating type hints for *.capnp schemas.

Notes:
    - The outputs of this generator are only compatible with pycapnp version >= 1.1.1.
    - Capnp interfaces (RPC) are not yet supported.
"""

from __future__ import annotations

from pathlib import Path
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Sequence

import os.path

import rich_click as click

from capnp_stub_generator.generator import run
from capnp_stub_generator.logging import log


class FileGlobs(click.types.StringParamType):
    name = "path/glob"


@click.command(context_settings={"help_option_names": ["-h", "--help"]})
@click.option(
    "--clean",
    "-c",
    type=FileGlobs(),
    multiple=True,
    default=[],
    show_default=True,
    help="path or glob expression that match files to clean up before stub generation.",
)
@click.option(
    "--paths",
    "-p",
    type=FileGlobs(),
    multiple=True,
    default=["**/*.capnp"],
    show_default=True,
    help="path or glob expression that match *.capnp files for stub generation.",
)
@click.option(
    "--excludes",
    "-e",
    type=FileGlobs(),
    multiple=True,
    default=[],
    show_default=True,
    help="path or glob expression to exclude from path matches.",
)
@click.option(
    "--work-dir",
    "-C",
    type=click.Path(exists=True, file_okay=False, resolve_path=True, path_type=Path),
    default=os.getcwd(),
    show_default=True,
    help="path to work on.",
)
@click.option(
    "--recursive",
    "-r",
    is_flag=True,
    default=False,
    show_default=True,
    help="recursively search for *.capnp files with a given glob expression.",
)
@click.option("-v", "--verbose", count=True, help="enable verbose output -v or -vv")
def main(
    clean: Sequence[str],
    paths: Sequence[str],
    excludes: Sequence[str],
    work_dir: Path,
    recursive: bool,
    verbose: int,
) -> int:
    """Generate type hints for capnp schema files."""
    log.set_verbosity(verbose)

    log.info("Working from root directory: %s", work_dir)

    run(clean, paths, excludes, recursive, work_dir)


if __name__ == "__main__":
    main()
